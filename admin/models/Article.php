<?php 
/**
    * Set an Article
 **/





 
 
 class Article {

    private $_id;
    private $_title;
    private $_content;
    private $_date;
    private $_writer;
    private $_image;
    private $_posted;

    public function __construct(array $data){
        $this->hydrate($data);
    }

    // 
    public function hydrate(array $data){
        foreach($data as $key => $value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this, $method))
            $this->$method($value);
        }
    }
    // ID
    public function setId($id){
        $id = (int) $id;
        if($id > 0){
            $this->_id = $id;
        }
    }
    //get ID
    public function id(){
        return $this->_id;
    }


    //title
    public function setTitle($title){
        if(is_string($title)){
            $this->_title = $title;
        }
    }
    public function title(){
        return $this->_title;
    }

    //content
    public function setContent($content){
        if(is_string($content)){
            $this->_content = $content;
        }
    }
    public function content(){
        return $this->_content;
    }

    //date
    public function setDate($date){
        $this->_date = $date;
    }
    public function Date(){
        return $this->_date;
    }

    //writer
    public function setWriter($writer){
        if(is_string($writer)){
            $this->_writer = $writer;
        }
    }
    public function writer(){
        return $this->_writer;
    }

    //medias
    public function setImage($image){
        if(is_string($image)){
            $this->_image = $image;
        }    
    }
    public function image(){
        return $this->_image;
    } 

    //etat
    public function setPosted($posted){
        $posted = (int) $posted;
        if($posted > 0){
            $this->_posted = $posted;
        }
    }
    public function posted(){
        return $this->_posted;
    }
    
}

