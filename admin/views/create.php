
<?php $this->_t = 'Publier'; ?>
<div class="container mt-3 mb-3 text-center">
<h4>Ecrire un article</h4>



          
    <form method="post" class="col-lg-12" enctype="multipart/form-data" >
        <!-- title -->
        <div class="form-group mt-1 mb-0 col-lg-12">
        <?php
                    if(isset($_POST['submit'])){
                        $errors = [];
                        // Gestion des erreurs post créé
                        if( empty($_POST['content']) && empty($_POST['title']) ){
                            $errors['emptyAll'] = 'Veuillez remplir tous les champs!';
                        }
                        else if(empty($_POST['title'])){
                            $errors['emptyTitle'] = 'Veuillez remplir le champ du titre<br>'; 
                        }
                        else if(empty($_POST['content'])){
                            $errors['emptyContent'] = 'Veuillez remplir le champ du contenu<br>';
                        }

                        if(!empty($errors)){
                            foreach($errors as $error){ ?>
                           
                           <div class="alert alert-danger">
                               <?php echo $error; ?>
                           </div><?php  
                        }       
                    }
                    }?>
            <input type="text" name="title" id="title" class="form-control title" placeholder="Titre de l'article"/>
            <label for="title"></label>
        </div>
        <!-- write -->
        <div class="form-group mt-1 mb-0 col-lg-12">
            <textarea type="text" id="content" name="content" class="md-textarea form-control content"  rows="12"></textarea>
            <label for="content"></label>
        </div>
        <div class="col-sm-12 col-lg-12">
            <div class="switch">
                <div class="custom-control custom-switch">
                    <input name="public" type="checkbox" class="custom-control-input" id="customSwitches">
                    <label id="dashboard--toggle" class="custom-control-label" for="customSwitches">Publié</label>
                </div>
            </div>
        </div>
        <!-- submit -->
        <div class="col-sm-12 col-lg-12">
            <button name="submit" type="submit"  class="btn btn-dark submit_btn col-sm-12 col-lg-3 text-light float-right">Publier</button>
        </div>
    </form>
</div> 
