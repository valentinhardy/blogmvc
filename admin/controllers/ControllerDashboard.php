<?php
/**
    * Display Dashboard items

    * viewDashboard : if $_SESSION = false : return error view
    * viewDashboard : display items by manager methods getAdmin getFront getArticles getDashboardComments
 **/

session_start();
require_once('./views/view.php');


class ControllerDashboard {

    private $_dashboardmanager;

    public function viewDashboard(){

        if($_SESSION == false){

            $errorMsg = "Vous n'avez pas les autorisations nécessaires.";
            $this->_view = new View('error');
            $this->_view->generate(array('errorMsg' => $errorMsg));

        } else {

            $this->_dashboardmanager = new DashboardManager();
            
            $admins = $this->_dashboardmanager->getAdmin(); 
            $dashboard = $this->_dashboardmanager->getFront(); 
            $articles = $this->_dashboardmanager->getArticles();
            $comments = $this->_dashboardmanager->getDashboardComments();

            $this->_view = new View('dashboard');
            $this->_view->generate(array('dashboard' => $dashboard, 'articles' => $articles, 'comments' => $comments, 'admins' => $admins));
        
        }

    } 
 
}