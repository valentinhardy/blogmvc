-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 17 juil. 2019 à 15:47
-- Version du serveur :  5.7.24
-- Version de PHP :  5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `blog`
--

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `comment` text CHARACTER SET utf8 NOT NULL,
  `post_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `seen` int(1) NOT NULL DEFAULT '0',
  `signaled` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=179 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `comments`
--

INSERT INTO `comments` (`id`, `name`, `email`, `comment`, `post_id`, `date`, `seen`, `signaled`) VALUES
(178, 'Valentin', 'Hardy@gmail.com', 'Coucou !', 1013, '2019-07-17 17:45:14', 0, 0),
(174, 'v', 'vv@gmail.com', 'ezfezf', 1012, '2019-07-17 16:43:38', 0, 1),
(175, 'Valentin', 'fdvfv@gmail.com', 'Commentaire', 1012, '2019-07-17 16:49:55', 0, 1),
(176, 'dvd', 'SDv@gmail.com', 'zfzqf', 1012, '2019-07-17 16:50:51', 0, 1),
(177, 'szdq', 'qq@gmail.com', 'ergerg', 1012, '2019-07-17 16:52:22', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `medias`
--

DROP TABLE IF EXISTS `medias`;
CREATE TABLE IF NOT EXISTS `medias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `file` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `type` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_medias_posts_idx` (`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `content` text CHARACTER SET latin1,
  `date` datetime DEFAULT NULL,
  `posted` int(11) NOT NULL DEFAULT '1',
  `writer` varchar(255) DEFAULT 'vhardy@insitaction.com',
  `image` varchar(255) NOT NULL DEFAULT 'post.png',
  PRIMARY KEY (`id`),
  KEY `fk_posts_users1_idx` (`writer`)
) ENGINE=MyISAM AUTO_INCREMENT=1020 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `date`, `posted`, `writer`, `image`) VALUES
(1016, 'Jour 5', '&lt;p&gt;Je n&rsquo;ai pas dormi cette nuit&period; &Eacute;puis&eacute; apr&egrave;s des heures d&rsquo;&eacute;critures passionn&eacute;es&comma; je d&eacute;cide de sortir fumer une cigarette sur le banc en limite de propri&eacute;t&eacute;&period; La nuit est glaciale et me procure un sentiment&nbsp; d&rsquo;apaisement&period; Calme et relax&eacute; donc&comma; je tire la derni&egrave;re bouff&eacute;e de ma cigarette quand une femme s&rsquo; approche de moi&period; Tr&egrave;s discr&egrave;te&comma; elle se trahit pourtant par le craqu&egrave;lement de la neige sous ses bottes fourr&eacute;es&period; Silencieuse toujours&comma; elle s&rsquo;assoit &agrave; cot&eacute; de moi&comma; et m&rsquo;offre un sursaut que je d&eacute;cide de faire passer pour un frisson&period; Apr&egrave;s de longues minutes muettes&comma;&nbsp; je brise le silence&period; Ouvrant mon paquet de cigarette&comma; je constate &agrave; voix haute qu&rsquo;il ne m&rsquo;en reste plus que deux&period; Alors que je mets une d&rsquo;entre elle &agrave; ma bouche&comma; elle me sourit et se saisit de la derni&egrave;re&period; Du reste &quest; Je me rappelle seulement de nombreux verres partag&eacute;s&comma; de discussions tant engag&eacute;es que passionnantes&period; &lt;strong&gt;Le tout achev&eacute; par de pudiques bonsoirs et une promesse de bient&ocirc;t&period;&nbsp;&nbsp;&lt;&sol;strong&gt;&lt;&sol;p&gt;\r&NewLine;&lt;p&gt;&nbsp;&lt;&sol;p&gt;', '2019-07-11 20:29:46', 1, 'vhardy@insitaction.com', 'post.png'),
(1015, 'Jour 4', '&lt;p&gt;Alors que j&rsquo;entame le quatri&egrave;me jour de mon voyage&comma; je n&rsquo;ai pas encore pris le temps de noircir le papier&period; Pourtant&comma; apr&egrave;s la r&eacute;initialisation de mes id&eacute;es ces derniers temps&comma; les projets ne cessent de se multiplier&period; Je me passionne doucement pour l&rsquo;univers de l&rsquo;enfance&period;&nbsp; Jusqu&rsquo;&agrave; ce jour&comma; je me sentais &lpar;par abus de pr&eacute;tention&rpar;&comma; trop incompr&eacute;hensible pour ces petits &ecirc;tres qui je dois l&rsquo;avouer&comma; me faisait peur&period; Je ne sais par quel moyen mais ce voyage a estomp&eacute; mes craintes et j&rsquo;ai aujourd&rsquo;hui envie d&rsquo;&eacute;crire pour eux&period; Un album de mon r&eacute;cit en Alaska pourrait &ecirc;tre l&rsquo;objet de mon travail pour les prochains mois&period; Les id&eacute;es fusent et le papier se chiffonne&period; J&rsquo;&eacute;teins mon t&eacute;l&eacute;phone et tout objet susceptible de me raccrocher en un instant &agrave; la r&eacute;alit&eacute;&period; Quelques vivres me suffiront&comma; j&rsquo;oublie &agrave; pr&eacute;sent la notion du temps&period; Les minutes ne comptent plus&comma; l&rsquo;ondulation des feuilles de papiers devient &lt;strong&gt;&lt;em&gt;ma pr&eacute;occupation&lt;&sol;em&gt;&lt;&sol;strong&gt;&period;&nbsp; &nbsp;&lt;&sol;p&gt;\r&NewLine;&lt;p&gt;&nbsp;&lt;&sol;p&gt;', '2019-07-11 20:29:24', 1, 'vhardy@insitaction.com', 'post.png'),
(1017, 'Jour 6', '&lt;p&gt;Mon retour est pr&eacute;vu pour aujourd&rsquo;hui&period; Dans mes souvenirs&comma; le vol est en d&eacute;but d&rsquo;apr&egrave;s-midi&period; Je ne suis plus s&ucirc;r de moi&period; A vrai dire&comma; je n&rsquo;ai pas songer au retour une seule fois pendant mon s&eacute;jour&period; L&rsquo;Alaska a d&eacute;rout&eacute; mon quotidien et ma passion d&rsquo;&eacute;crire&period; Perdu au moment de l&rsquo;embarquement je d&eacute;cide de renoncer&period; La possibilit&eacute; d&rsquo;&eacute;crire pour toujours devient une &eacute;vidence&period; Pour faire demi-tour&comma; je pr&eacute;texte une urgence vitale&period; Ce n&rsquo;est pas mentir&comma; il s&rsquo;agit en effet de sauver ma vie imm&eacute;diatement&period; L&rsquo;h&ocirc;tesse de l&rsquo;air m&rsquo;informe que si je ne monte pas &agrave; bord il me sera impossible&nbsp; de r&eacute;cup&eacute;rer ma valise&period; Je n&rsquo;h&eacute;site pas et franchit la barri&egrave;re&period; Rien ne me raccroche plus &agrave; la vie que ma pr&eacute;sence ici&comma; et encore moins mes affaires souill&eacute;es dans ma valise&period; A pleine allure dans le hall de l&rsquo;a&eacute;roport&comma; je cours trouver un endroit calme pour &eacute;crire&period; La suite de mon histoire et de mon &oelig;uvre commence ici&period;&nbsp;&lt;&sol;p&gt;', '2019-07-11 20:30:05', 1, 'vhardy@insitaction.com', 'post.png'),
(1012, 'Jour 1', '&lt;p&gt;&lt;em&gt;Situ&eacute; au Nord-Ouest du &lt;strong&gt;Canada&lt;&sol;strong&gt;&comma; l&rsquo;Alaska est le plus grand Etat des Etats-Unis&period; Bien loin des buildings et du fourmillement am&eacute;ricain&comma; la diversit&eacute; de ses paysages aux couleurs froides a su faire fantasmer ma plume d&rsquo;&eacute;crivain&period; Si l&rsquo;Alaska est la destination parfaite pour les sportifs en qu&ecirc;te de sensations fortes&comma; la raison de mon voyage est bien plus primaire&period; Les d&eacute;lais s&rsquo;amenuisent et je n&rsquo;ai plus qu&rsquo;une solution &colon; saisir le papier et le crayon&period; Il serait un peu facile de rappeler les liens complexes entre le bonheur et l&rsquo;argent mais telle qu&rsquo;est la situation &agrave; ce jour&comma; pour survivre il me faut &eacute;crire&period; Ma volont&eacute; n&rsquo;est pas d&rsquo;incriminer le fonctionnement de la culture du travail de notre pays&comma; alors tentons d&rsquo;oublier le pourquoi du comment&period;&nbsp; En mal d&rsquo;inspiration et avec la soif d&rsquo;aventures&comma; j&rsquo;ai donc choisi de partir &agrave; la rencontre du plus profond de l&rsquo;Alaska&period;&nbsp; A l&rsquo;heure ou je pose ses mots&comma; l&rsquo;avion d&eacute;colle &colon; une demi journ&eacute;e de voyage avant mes premiers pas sur la terre Alaskaine&period;&nbsp;&nbsp;&lt;&sol;em&gt;&lt;&sol;p&gt;\r&NewLine;&lt;p&gt;&lt;em&gt;&nbsp;&lt;&sol;em&gt;&lt;&sol;p&gt;', '2019-07-11 20:28:08', 1, 'vhardy@insitaction.com', 'post.png'),
(1013, 'Jour 2', '&lt;p&gt;Malgr&eacute; le froid du climat&comma; le r&eacute;veil est brulant ce matin&period; R&eacute;veill&eacute; par la lumi&egrave;re du jour&comma; je n&rsquo;ai aucune id&eacute;e de l&rsquo;heure qu&rsquo;il est&period; L&rsquo;Alaska est un pays d&rsquo;un autre temps mais surtout&comma; le pays o&ugrave; l&rsquo;on peut prendre le temps de vivre chaque minute de sa vie&period;&nbsp; Je dois l&rsquo;avouer&comma; je suis compl&egrave;tement gris&eacute; par l&rsquo;atmosph&egrave;re du pays&period; Habitu&eacute; par le remous de nos grandes villes fran&ccedil;aise&comma; le calme de l&rsquo;Alaska est assommant&period; Une perte de connaissance pour une renaissance des plus heureuse&period; Ici&comma; je me sens libre de regarder&comma; d&rsquo;entendre et de sentir&period; Chaque sens est lib&eacute;r&eacute;&comma; ind&eacute;pendant de cette stimulation oppressante &agrave; laquelle je m&rsquo;&eacute;tais habitu&eacute;&period; Ce matin au petit-d&eacute;jeuner&comma; le go&ucirc;t de mon caf&eacute; est diff&eacute;rent&comma; plus fort&period; La mie de ma tartine est plus tendre&comma; r&eacute;confortante&period; Mes sensations semblent chang&eacute;es&period;&nbsp; Hier&comma; en sortant de l&rsquo;avion&comma; le froid a brul&eacute; ma peau&period; Ce matin&comma; il la caresse&period; J&rsquo;ai l&rsquo;impression de tomber amoureux&period; Une chose est sure&comma; je suis soumis &agrave; mon premier coup de foudre&period;&nbsp;&nbsp;&lt;&sol;p&gt;\r&NewLine;&lt;p&gt;&nbsp;&lt;&sol;p&gt;', '2019-07-11 20:28:27', 1, 'vhardy@insitaction.com', 'post.png'),
(1014, 'Jour 3', '&lt;p&gt;&amp;lt;p&amp;gt;L&amp;amp;rsquo;Alaska est l&amp;amp;rsquo;Etat le moins dens&amp;amp;eacute;ment peupl&amp;amp;eacute; des Etats-Unis. Bien &amp;amp;eacute;videmment, la quantit&amp;amp;eacute; n&amp;amp;rsquo;est pas gage de qualit&amp;amp;eacute; et la population alaskaine est une belle illustration de ce c&amp;amp;eacute;l&amp;amp;egrave;bre adage. Ce matin, j&amp;amp;rsquo;ai d&amp;amp;eacute;cid&amp;amp;eacute; de partir en d&amp;amp;eacute;couverte sans feuille de route ni plan pr&amp;amp;eacute;d&amp;amp;eacute;fini. Alors qu&amp;amp;rsquo;arrive l&amp;amp;rsquo;heure du d&amp;amp;eacute;jeuner, la faim me tiraille peu &amp;amp;agrave; peu le ventre. Quant &amp;amp;agrave; mon cheminement improvis&amp;amp;eacute;, il devient de plus en plus incertain et l&amp;amp;rsquo;id&amp;amp;eacute;e d&amp;amp;rsquo;un bon repas dispara&amp;amp;icirc;t progressivement. &amp;amp;Eacute;puis&amp;amp;eacute;, je d&amp;amp;eacute;cide de m&amp;amp;rsquo;arr&amp;amp;ecirc;ter quelques instants aux abords d&amp;amp;rsquo;un village qui me semble abandonn&amp;amp;eacute;. Je ferme les yeux avant d&amp;amp;rsquo;&amp;amp;ecirc;tre r&amp;amp;eacute;veill&amp;amp;eacute; en sursaut par des rires : ceux des habitants de ce hameau. Ils semblent presque m&amp;amp;rsquo;attendre pour le d&amp;amp;eacute;jeuner. Je passe la journ&amp;amp;eacute;e &amp;amp;agrave; leurs cot&amp;amp;eacute;s, une rencontre r&amp;amp;eacute;g&amp;amp;eacute;n&amp;amp;eacute;rante. En raconter davantage sur eux serait trahir l&amp;amp;rsquo;esquisse de mes prochaines publications. Tout est d&amp;amp;eacute;j&amp;amp;agrave; &amp;amp;eacute;crit dans mon esprit.&amp;amp;nbsp;&amp;lt;/p&amp;gt;&lt;/p&gt;', '2019-07-11 20:28:44', 1, 'vhardy@insitaction.com', 'post.png');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `token` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL DEFAULT 'Modo',
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `token`, `role`, `name`) VALUES
(1, 'hardy.valpro@gmail.com', '$2y$10$EV9u0JMbwEQQSzx3YTRtuughA3.Mg9a3LUxJEyEoa2a9Ybeun.NBK', 'tfiugugyg', 'moderateur', 'Valentin'),
(2, 'vhardy@insitaction.com', 'password1', 'rokgzp5515', 'moderateur', 'Test');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
