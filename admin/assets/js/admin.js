
$(document).ready(function(){


  //  TINY MCE 
  tinymce.init({
      entity_encoding : "raw",
      selector:'textarea',
      height: 500,
    });

  //  AJAX
  $('.deleteComment').on('click', function(){
    console.log('Commentaire supprimé');
    var id = $(this).attr('id');
    $.post('../admin/functions/DeleteComment.php',{id:id}, function(){
    $('.comment_'+id).hide();
    });
  });

  // Delete un post
  $('.deletePost').on('click', function(){
    console.log('Article supprimé');
    var id = $(this).attr('id');
    $.post('../admin/functions/DeletePost.php',{id:id}, function(){
    $('.post_'+id).hide();
    });
  });

  // Validate un post
  $('.validateComment').on('click', function(){
    console.log('Commentaire validé');
    var id = $(this).attr('id');
    $.post('../admin/functions/ValidateComment.php',{id:id}, function(){
    $('.comment_'+id).hide();
    });
  });


//   function Validator(){

//     $title = $('.title');
//     $content = $('.content');

//     if($title == null){
//       alert('titre vide !');
//     }
//     if($content == null ){ 
//       alert ('contenu vide !');
//     }
//   }
//   $('.submit_btn').on('click', function(){
//     Validator();
//   })
 })