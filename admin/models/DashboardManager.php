<?php 
/**
    * Return dashboard items

    * getFront : connexion bdd 
    * getArticles : search table 'posts' for setting Article
    * getDashboardComment : return signaled comments
    * getAdmin : return all users
 **/


class DashboardManager extends Model{


    public function getFront(){
        $this->getBdd(); 
    }

    public function getArticles(){
        $this->getBdd();
        return $this->getAll('posts', 'Article');
    }

     //recupere tous les commentaires seen = 0
     public function getDashboardComments(){
        //fonction model
        $req = $this->getBdd()->prepare("SELECT * FROM comments WHERE signaled='1' ORDER BY date DESC");
        $req->execute(array());
        $result  = [];

        while($rows = $req->fetchObject()){
            $result[] = $rows;
        }
        return $result;
    }

    public function getAdmin(){
        $req = $this->getBdd()->prepare("SELECT * FROM users ORDER BY id  DESC");
        $req->execute(array());
        $result  = [];

        while($rows = $req->fetchObject()){
            $result[] = $rows;
        }
        return $result;
    }

}