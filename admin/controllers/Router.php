<?php 
require_once('views/view.php');

class Router {

    public function routeReq(){
        try {
            //chargement auto des classes // spl_autoload_register->repere le nom de la classe automatiquement
            spl_autoload_register(function($class){ 

                if(substr($class, 0, 10) === 'Controller'){
                    require_once('controllers/'.$class.'.php');
                } 
                else {
                require_once('models/'.$class.'.php');
                }

            });

            $url = '';
            // Controller est inclu selon l'action de l'utilisateur
            if(isset($_GET['url'])) {   //Recup les parametres de manière separées par un / puis filtre 
                $url = explode('/', filter_var($_GET['url'], FILTER_SANITIZE_URL));

                switch ($_GET['url']) {
                    case 'connexion':
                        $_loginController = new ControllerConnexion();
                        $_loginController->viewConnexion();
                    break;

                    case 'connecting':
                        if (isset($_POST['submit']) || isset($_POST['password']) || isset($_POST['email'])) {

                            $email = htmlspecialchars(trim($_POST['email']));
                            $password = htmlspecialchars($_POST['password']);
                            $_connexionController = new ControllerConnexion();
                            $_connexionController->connexion($email,$password);

                        }
                    break;

                    case 'logout':
                        $logoutController = new ControllerConnexion();
                        $logoutController->viewLogout();
                    break;
                    
                    case 'dashboard':
                        $_dashboardController = new ControllerDashboard();
                            $_dashboardController->viewDashboard();
                
                    break;
                    
                    case 'edit':
                        if(isset($_GET['id'])){
                        
                            $_editController = new ControllerEdit();
                            $_editController->post($_GET['id']); 

                            if(!empty($_POST)){

                                $_editController = new ControllerEdit();
                                $_editController->editPost($_GET['id']);
    
                            }

                        } else {

                            $errorMsg = "Certains paramètres sont manquants afin d'accèder à la requête.";
                            $this->_view = new View('error');
                            $this->_view->generate(array('errorMsg' => $errorMsg));

                        }
                    break;
    
                    case 'create':
                        if( !isset($_GET['id'])){

                            $_createController = new ControllerCreate();
                            $_createController->viewCreate();

                            if(!empty($_POST)){

                                $_creatingController = new ControllerCreate();
                                $_creatingController->createPost();

                                }
                                
                        } else {

                            $errorMsg = "Certain paramètres sont manquants afin d'accèder à la requête. ";
                            $this->_view = new View('error');
                            $this->_view->generate(array('errorMsg' => $errorMsg));

                        }
                    break;

                    default:
                        throw new Exception('Page introuvable');
                    break;
                }
                    
            } else {

                header ('location: index.php?url=connexion');

            }
        }
        //Gestion des erreurs
        catch(Exception $e)
        {
            $errorMsg = $e->getMessage();
            $this->_view = new View('error');
            $this->_view->generate(array('errorMsg' => $errorMsg));
        }
    }
}
