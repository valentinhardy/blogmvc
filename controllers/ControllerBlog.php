<?php session_start();

/**
 *  articles : get all articles where posted -> 1
 */


require_once('./views/view.php');

class ControllerBlog
{
    private $_articleManager;
    private $_view;

    public function articles() 
    {
        $this->_articleManager = new ArticleManager;
        $articles = $this->_articleManager->getArticles();
        
        $this->_view = new View('blog');
        $this->_view->generate(array('articles' => $articles));
    }
}