<?php
/**
    * Connect an admin

    * View / Connect an admin / Logout an admin
    * connexion($params)
    * user($email) : execute if user.email = $_POST['email] 
 **/

require_once('./views/view.php');



class ControllerConnexion {
    private $_loginManager;

    public function connexion($email,$password){

        $this->_loginManager = new ConnexionManager();

        $admin = $this->_loginManager->user($email);

        $admin = $admin->fetch();
        
        if(password_verify($password, $admin['password'])){

            session_start();
            $_SESSION['admin'] = $admin;
            header ('location: index.php?url=dashboard');

        } else {

            $connexion = $this->_loginManager->getConnexion();
            $this->_view = new View('connexion');
            $this->_view->generate(array('connexion' => $connexion, 'email' => $email, 'password' => $password, 'user' => $user));
        
        }
        return $admin;
    }
    
    public function viewConnexion(){
        
        $this->_loginManager = new ConnexionManager();

        $connexion = $this->_loginManager->getConnexion();

        $this->_view = new View('connexion');
        $this->_view->generate(array('connexion' => $connexion));
    }

    public function viewLogout(){

        $this->_logoutManager = new ConnexionManager();

        $logout = $this->_logoutManager->getConnexion();

        //deconnecte la session
        session_start();
        unset($_SESSION['admin']);
        unset($_SESSION['name']);
        
        if(isset($_SESSION['admin'])){

            die ('Vous êtes connécté');

        } else {

            header('Location: ../index.php?url=home');
            die("Vous êtes déconnecté");

        }
    
        $this->_view = new View('logout');
        $this->_view->generate(array('logout' => $logout));
    }
}
