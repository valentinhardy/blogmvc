<?php 

    $this->_t = $post->title; ?>
 <div class="container">
     <div class="row">
        <div class="col-lg-12">
            <header class="masthead text-center">
                <div class="masthead-content">
                    <div class="container">
                        <h1 class="masthead-heading mb-0"><?php echo $post->title;?></h1>
                        <h4>Voyage au bout du monde</h4>
                    </div>
                </div>
            </header>
         
                <!-- ARTICLE -->
                <div class=" col-lg-12  text-dark bg-light pt-3 pb-3">
                    <div class="masthead" style="background-image: url('./assets/img/<?php echo $post->image?>');"></div>

                    <div class="post-content"><?php echo $post->content ?></div>
                </div>
       
        </div>
    </div>
    <div class="row">
            <!-- COMMENTER & COMMENTAIRES -->
        <div class="col-lg-6 mt-5 mb-3">
                <h5 class="post_article--title">Commenter</h5>
            
                <form method="post" class="row">
            
                    <div class="form-group col-lg-12 col-md-12 mt-1 mb-0">
                    <?php   
                if (!empty($errors)){?>
                <div class="alert alert-danger">
            <?php 
                foreach($errors as $error){
                        echo $error.'<br>';
                    } ?>
                </div>
            <?php
                }
                ?>
                    <input type="text" name="name" id="name" placeholder="Nom" class="form-control" />
                    <label for="name"></label>
                </div>
                <div class="form-group col-lg-12 col-md-12 mt-1 mb-0">
                    <input type="email" name="email" id="email" placeholder="Adresse E-mail" class="form-control" />
                    <label for="email"></label>
                </div>
                <div class="form-group col-lg-12 mt-1 mb-0">
                    <textarea class="form-control" name="comment" placeholder="Commentaire"
                        id="exampleFormControlTextarea1" rows="3"></textarea>
                    <label for="comment"></label>
                </div>
        
                    <button href="#" type="submit" name="submit" class="btn-dark text-light send-post">Envoyer</button>          
                                 
            </form>
        
        </div>
        <!-- AFFICHE LES COMMENTAIRES -->
        <div class="col-lg-6 mt-5 mb-3">
            <h5 class="post_article--title">Commentaires</h5>

            <div class="list-group ">
            
            
                <?php foreach($comments as $comment): ?>
                <div id="comment_<?php echo $comment->id;?>"
                    class="list-group-item list-group-item-action flex-column align-items-start">
                    <div class="d-flex w-50 justify-content-between">
                        <h5 class="mb-1"><?php echo  $comment->name?></h5>
                        <small><?php echo  date("d/m/Y", strtotime($comment->date)) ?></small>
                    </div>
                    <p><?php echo $comment->comment;?></p>
                    <small id="<?php echo $comment->id;?>" class="signalComment">Signaler</small>
                </div>
                <?php endforeach; ?>
            </div>
            <?php if (empty($comments)){
            echo "<div class='alert alert-warning'>Aucun commentaire n'a été publié, serez-vous le premier?</div>";
            }
            ?>
        </div>
    </div>
</div>
