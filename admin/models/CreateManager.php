<?php
/**
    * Send Article to bdd

    * getCreate : connexion bdd
    * writePost : insert $title $content $posted and create a new article
 **/

 


 class CreateManager extends Model {

    public function getCreate(){
        $this->getBdd(); //function article manager 
    }

    public function writePost($title,$content,$posted){
    
        $a = [
            'title'   => $title, 
            'content' => $content,
            'posted'  => $posted
        ];

        $requete = "INSERT INTO posts (title, content, posted, date) VALUES (:title, :content, :posted, NOW())";
        $req = $this->getBdd()->prepare($requete);
        
        $content = $req->execute($a);
    } 
}
