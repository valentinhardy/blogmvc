<?php
/** 
    * Edit a post

    * if(isset($_SESSION['admin'])) : verify the current session / else : return error view
    * post : return post view by id, if id doesn't exist : view error
    * editpost : update post by id with updatePost method 
**/

session_start();
require_once('./views/view.php');


class ControllerEdit{
 
    public function post($id) {

        if(isset($_SESSION['admin'])){
            
            $this->_editManager = new EditManager();

            $post = $this->_editManager->getPost($id);

            if($post == true && is_numeric($_GET['id'])) {

                $this->_view = new View('edit');
                $this->_view->generate(array('post' => $post));

            } else {

                $errorMsg = 'Vous souhaitez éditer un article inexistant.';
                $this->_view = new View('error');
                $this->_view->generate(array('errorMsg' => $errorMsg));

            }
        } else {

            $errorMsg = 'Vous n\'avez pas les autorisations nécessaires.';
            $this->_view = new View('error');
            $this->_view->generate(array('errorMsg' => $errorMsg));

        }
    }
  

    public function editPost($id){

        if(isset($_SESSION['admin'])){
    
            $title = htmlspecialchars(trim($_POST['title']));
            $content = strip_tags(htmlentities($_POST['content'], ENT_HTML5 , 'UTF-8'));
            $posted = isset($_POST['public']) ? "1" : "0";

            if(!empty($title) && !empty($content)){

                $this->_editManager = new EditManager();

                $this->_editManager->updatePost($id,$title,$content,$posted);
                header('Location: index.php?url=dashboard');

            } else { 

                echo "<div class='alert alert-warning text-center'>Oups ! Vous ne pouvez pas envoyer un article vide.</div>";
            
            }

        } else {

            $errorMsg = 'Vous n\'avez pas les autorisations nécessaires pour éditer un article.';
            $this->_view = new View('error');
            $this->_view->generate(array('errorMsg' => $errorMsg));

        }
    }
}
