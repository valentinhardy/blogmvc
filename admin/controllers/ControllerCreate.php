<?php 
/**
    * Create new Article

    * viewCreate : set create view
    * createPost : create a new post by $_POST['*****'] with writePost method 
 **/

session_start();
require_once('./views/view.php');


class ControllerCreate{

    public function viewCreate(){

        if(isset($_SESSION['admin'])){

        $this->_createManager = new CreateManager();

        $create = $this->_createManager->getCreate(); 

        $this->_view = new View('create');
        $this->_view->generate(array('create' => $create));

        } else {

            $errorMsg = 'Vous n\'avez pas les autorisations nécessaires.';
            $this->_view = new View('error');
            $this->_view->generate(array('errorMsg' => $errorMsg));

        }
    }

    public function createPost(){

        $title = htmlspecialchars(trim($_POST['title']));
        $content = strip_tags(htmlentities($_POST['content'], ENT_HTML5 , 'UTF-8'));
        $posted = isset($_POST['public']) ? "1" : "0";

        if(isset($_SESSION['admin'])){

            if(!empty($title) && !empty($content)){

                $this->_createManager = new CreateManager();

                $this->_createManager->writePost($title,$content,$posted);
                header('Location: index.php?url=dashboard');

            }
            
        } else {

            $errorMsg = 'Vous n\'avez pas les autorisations nécessaires.';
            $this->_view = new View('error');
            $this->_view->generate(array('errorMsg' => $errorMsg));
            
        }
     
    }
}