<?php 
 $this->_t = 'Editer' ?>

        <div class="container text-center mb-3 mt-3">
                <h4>Editer <?php echo $post->title?></h4>
               
    <form method="post"  class="col-lg-12" enctype="multipart/form-data" >
        <!-- title -->
        <div class="form-group mt-1 mb-0 col-lg-12">
        <?php 
        if(isset($_POST['submit'])){

            $errors = [];
            // Gestion des erreurs post créé
            if( empty($_POST['content']) && empty($_POST['title']) ){
                $errors['emptyAll'] = 'Veuillez remplir tous les champs!';
            }
            else if(empty($_POST['title'])){
                $errors['emptyTitle'] = 'Veuillez remplir le champ du titre<br>'; 
            }
            else if(empty($_POST['content'])){
                $errors['emptyContent'] = 'Veuillez remplir le champ du contenu<br>';
            }

                if(!empty($errors)){
                    foreach($errors as $error){ ?>
                
                <div class="alert alert-danger">
                    <?php echo $error; ?>
                </div><?php  
                }       
            }
        } ?>
            <input type="text" name="title" id="title" value="<?php echo $post->title;?>" class="form-control" placeholder="Editer le titre"/>
            <label for="title"></label>
        </div>
        <!-- write -->
        <div class="form-group mt-1 mb-0 col-lg-12">
            <textarea type="text" id="content" name="content" class="md-textarea form-control" placeholder="editer un article" rows="12"><?php echo $post->content?></textarea>
            <label for="content"></label>
        </div>
        <div class="col-sm-12 col-lg-12">
            <div class="switch">
                <div class=" col-lg-2 custom-control custom-switch">
                    <input name="public" <?php if($post->posted === '1'){ ?>checked="checked" <?php } ?> type="checkbox" class="custom-control-input" id="customSwitches">
                    <label id="dashboard--toggle" class="custom-control-label" for="customSwitches">Publié</label>
                </div>
            </div>
            <button name="post" type="submit" class="btn btn-dark col-sm-12 col-lg-2 text-light float-right">Publier</button>
        </div>
        <!-- submit -->
      
    </form>
</div> 
<?php    
       
    ?>
