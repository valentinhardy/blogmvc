<?php
$this->_t = 'Dashboard';
?>
    <div class="container-fluid">
        <h4 class="float-center text-dark text-center blog_post--infos mt-2 mb-2">Dashboard</h4>
        <div class="alert text-center alert-success">
            <?php if(isset($_SESSION['admin'])){ ?>
            <h5> Bonjour
                <?php echo  $_SESSION['admin']['name']; ?> !
            </h5>
        </div>
        <div class="alert text-center alert-warning white-text">
                <h5 class="text-left">Profil<br></h5>
                <p class="text-left">Nom : <?php echo  $_SESSION['admin']['name']; ?></p>
                <p class="text-left">E-Mail : <?php echo  $_SESSION['admin']['email']; ?></p>
                <p class="text-left">Token : <?php echo  $_SESSION['admin']['token']; ?></p>
                <p class="text-left">Role : <?php echo  $_SESSION['admin']['role']; ?></p>
        </div>
        <?php  }?>
    </div>

<div class="container-fluid box-articles">
   
    <div class="row">
      
        <!-- Tableau Articles -->
        <div class="container-fluid">

        <div class="alert text-left alert-success">
           <h4>Articles</h4>
        </div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="text-center" scope="col">#</th>
                        <th class="text-center" scope="col">Titre</th>
                        <th class="text-center" scope="col">Auteur</th>
                        <th class="text-center" scope="col">Date</th>
                        <th class="text-center" scope="col">Etat</th>
                        <th class="text-center" scope="col">Editer</th>
                        <th class="text-center" scope="col">Supprimer</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($articles as $article):?>
                        <tr class="post_<?php echo $article->id();?>">
                            <th class="text-center" scope="row"><?php echo $article->id();?></th>
                            <td class="text-center pt-1 pb-1"><?php echo $article->title();?></td>
                            <td class="text-center pt-1 pb-1"><?php echo $_SESSION['admin']['name'];?></td>
                            <td class="text-center pt-1 pb-1"><?php echo date('d/m/Y à H:i', strtotime($article->date()))?></td>
                            <td class="text-center"><?php if ($article->posted() === 1){
                                echo '<span class="badge p-2 badge-success">Publié</span>';
                            } else {
                                echo '<span class="badge p-2 badge-warning text-white">Non publié</span>';
                            }?>
                            </td>
                            <td class="text-center pt-3 pb-3"><a href="edit&id=<?php echo $article->id();?>" alt="editer un article"><i class="fas fa-edit"></i></a>
                            </td>
                            <td  class=" text-center pt-3 pb-3"><span id="<?php echo $article->id()?>" class="deletePost text-light badge p-2 badge-danger">Supprimer</span></td>
                        </tr>
                    <?php endforeach;?>
                <tbody>

                </tbody>
            </table>
        </div>

          <!-- Tableau commentaires -->
          <div class="container-fluid">
          <div class="alert text-right alert-success">
           <h4>Commentaires signalés</h4>
        </div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="text-center" scope="col">#</th>
                        <th class="text-center" scope="col">Nom</th>
                        <th class="text-center" scope="col">Date</th>
                        <th class="text-center" scope="col">Commentaire</th>
                        <th class="text-center" scope="col">Etat</th>
                        <th class="text-center" scope="col">Supprimer</th>
                        <th class="text-center" scope="col">Valider</th>


                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($comments as $comment): ?>
                    <tr class="comment_<?php echo $comment->id;?>">
                        <th class="text-center" scope="row"><?php echo $comment->id;?></th>
                        <td class="text-center pt-1 pb-1"><?php echo $comment->name;?></td>
                        <td class="text-center pt-1 pb-1"><?php echo date('d/m/Y à H:i', strtotime($comment->date))?></td>  
                        <td class="text-center pt-1 pb-1"><?php echo $comment->comment;?></td>  
                        <td class="text-center pt-3 pb-3">
                        <?php if($comment->signaled === '1'){
                            echo '<span class="badge p-2 text-light badge-warning">Signalé</span>';
                        }?>
                        </td>
                        <td  class=" text-center pt-3 pb-3"><span id="<?php echo $comment->id;?>" class="deleteComment text-light badge p-2 badge-danger">Supprimer</span></td>
                        <td  class=" text-center pt-3 pb-3"><span id="<?php echo $comment->id;?>" class="validateComment badge p-2 badge-success">Valider</span></td>

                    </tr>
            <?php endforeach;?>
            </table>

        </div>
    </div>
</div>

