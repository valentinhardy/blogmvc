<?php 

class View {
    private $_file;
    private $_t;

    public function __construct($action){
        
        $this->_file = 'views/'.$action.'.php';

    }
    //recupere les data & affiche la vue
    public function generate($data){
        //fait passer la vue affichée avec les données de la vue
        $content = $this->generateFile($this->_file, $data);

        // template
        $view = $this->generateFile('views/template.php', array('t' => $this->_t,
        'content' => $content));
         echo $view;
    }

    //genere le fichier vue et renvoie le rslt
    private function generatefile($file, $data){
        if(file_exists($file)){

            extract($data);

            ob_start(); //mise en tampon

            //fichier vue
            require $file;

            return ob_get_clean();
            
        }else{
            throw new Exception('Fichier '.$file.' introuvable');
        }
    }
}