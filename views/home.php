
<?php  $this->_t = 'Jean Forteroche - Accueil'; ?>
<header class="masthead text-center">
    <div class="masthead-content">
        <div class="container">
            <h1 class="masthead-heading mb-0">Voyage en Alaska</h1>
            <h2 class="masthead-subheading mb-0">Au coeur de notre monde.</h2>
            <a href="/blog" class="btn btn-primary btn-xl rounded-pill mt-5">Voir le blog</a>
        </div>
    </div>
</header>

<section class="owl-carousel__container">
    <div class="owl-carousel">
        <?php foreach($articles as $article){ ?>
        <div>
            <div class="home_articles--items container">
                <a href="/post&id=<?php echo $article->id();?>">
                    <div class="row align-items-center text-center">
                            <div class="p-2">
                                <h3 class="home_articles--title display-6"><?php echo  $article->title()?></h3>
                                <img class="img" src="./assets/img/<?php echo  $article->image()?>" alt="">
                            </div>
                    </div>
                </a>
            </div>
        </div>
        <?php } ?>
    </div>
</section>