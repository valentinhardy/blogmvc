<?php 
/**
    * Comments

    * sendComment : insert comments from post method -> $sentComment['name'] = $_POST['name] 
    * getComments : select comments where post_id = $_GET['id'] (post&id=xxxxx) and signaled = 0
 **/ 


 class CommentsManager extends Model {
    
    public function sendComment($id,$name,$email,$comment){
        $sentComment = array(
            'name'    => $name,
            'email'   => $email,
            'comment' => $comment,
            'post_id' => $id
        );
        $sql = "INSERT INTO comments(name,email,comment,post_id,date) VALUES (:name, :email, :comment, :post_id, NOW())";
        
        $req = $this->getBdd()->prepare($sql);
        $req->execute($sentComment);
        header ('Location: index.php?url=post&id='.$id);
    } 
    

    public function getComments($id){
        //fonction model
        $req = $this->getBdd()->prepare("SELECT * FROM comments WHERE post_id = ? AND signaled='0' ORDER BY date DESC");
        $req->execute(array($id));
        $result  = [];

        while($rows = $req->fetchObject()){
            $result[] = $rows;
        }
        return $result;
    }
}