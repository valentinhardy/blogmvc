$(document).ready(function(){

    //Blog
    $(".owl-carousel").owlCarousel({
        items: 1,
        loop: true,
        dots: false,
        center: true,
        responsive:{
            600:{
                items:2
            },
            425:{
                items: 1
            }
        },
        margin: 0,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplaySpeed: 1000,
    });
    //Post
    $(".owl-carousel").owlCarousel({
        items: 1,
        loop: true,
        dots: false,
        center: true,
        responsive:{
            600:{
                items:5
            },
            425:{
                items: 1
            }
        },
        margin: 0,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplaySpeed: 1000,
    });
    

    $('.top').css('height', $('.navbar-custom').height());
    
    $('.grid').css('padding-top', '50px');
    $('.grid').masonry({
        // set itemSelector so .grid-sizer is not used in layout
        itemSelector: '.grid-item',
        // use element for option
        percentPosition: true
    })

    $('.signalComment').on('click', function(){
        console.log('commentaire signalé');
        var id = $(this).attr('id');
        $.post('../../functions/SignalComment.php',{id:id}, function(){
        $('#comment_'+id).hide();
        });
    });

});

