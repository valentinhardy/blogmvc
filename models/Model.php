<?php
/**
    * Models

    * setBdd : set Bdd informations 
    * getBdd : return bdd connexion for xxxxxxManager
    * getAll : getAll( name table , $object ) / ('post','Articles), ('post', 'Comments') etc
    * getOne : get a post by Id ($_GET['id'])
 **/ 
class Model {

    private static $bdd;


    public function setBdd(){
        $dbhost = 'localhost';
        $dbname = 'blog';
        $dbuser = 'root';
        $dbpswd = '';  
        self::$bdd= new PDO('mysql:host='.$dbhost.';dbname='.$dbname,$dbuser,$dbpswd,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
    }
    
    public function getBdd(){
        if(self::$bdd == null){
            $this->setBdd();
        }
        return self::$bdd;
    }

    protected function getAll($table,$obj){
        $var = [];
        $req = $this->getBdd()->prepare("SELECT * FROM ".$table." WHERE posted='1' ORDER BY id desc");
        $req->execute();
        
        while($data = $req->fetch(PDO::FETCH_ASSOC)){
            $var[] = new $obj($data);
        }
        
        return $var;
        $req->closeCursor();
    }

    //RECUP un article
    protected function getOne($id){
        //requête
        $sql = "SELECT * FROM posts WHERE id = ('$id') AND posted='1'";
        $req = $this->getBdd()->prepare($sql);
        $req->execute(array($id));
        $result = $req->fetchObject();

        return $result;
    }
}
