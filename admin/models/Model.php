<?php
/**
    * Models

    * setBdd : set Bdd informations 
    * getBdd : return bdd connexion for xxxxxxManager
    * getAll : getAll( name table , $object ) / ('post','Articles), ('post', 'Comments') etc
    * getOne : get a post by Id ($_GET['id'])
    * updatePost : set $id $title $content $posted in bdd
 **/ 

class Model { //ne peut pas être instanciée

    private static $bdd;

    // Connexion bdd
    public function setBdd(){

        $dbhost = 'localhost';
        $dbname = 'blog';
        $dbuser = 'root';
        $dbpswd = '';  

        self::$bdd= new PDO('mysql:host='.$dbhost.';dbname='.$dbname,$dbuser,$dbpswd,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
        
    }
    
  
    public function getBdd(){
        if(self::$bdd == null){
            $this->setBdd();
        }
        return self::$bdd;
    }

 
    protected function getAll($table,$obj){
        $var = [];
        $req = $this->getBdd()->prepare("SELECT * FROM ".$table." ORDER BY date ASC");
        $req->execute();
        
        while($data = $req->fetch(PDO::FETCH_ASSOC)){
            $var[] = new $obj($data);
        }
        
        return $var;
        $req->closeCursor();
    }


    protected function getOne($id){
       
        $req = $this->getBdd()->prepare("SELECT * FROM posts WHERE id=? ");
        $req->execute(array($id));

        $result = $req->fetchObject();
        
        return $result;
    
    } 

    

}