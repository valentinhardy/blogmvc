<?php session_start();
$this->_t = 'Connexion';
?>

<?php
if (isset($_SESSION['admin'])) {
    header('location: dashboard');
}
?>



<div class="connexion_form--item col-md-12 col-xs-12 col-lg-6 mt-2 p-5 m-auto">
    <div class="col-md-12 login-form-1 mt-3 mb-3">
        <h3 class="m-5 text-center">Connexion<i class=" ml-3 fab fa-quinscape fa-1x"></i></h3>
        <?php $errors = [];

                    if(isset($_POST['submit'])){

                        if (empty($_POST['email']) || empty($_POST['password'])){

                            $errors['empty'] = "Veuillez remplir tous les champs";

                        }

                        else if(!password_verify($password, $user['password'])){

                            $errors['exist'] = "Utilisateur inconnu.";

                        }

                        if (!empty($errors)){

                            echo '<div class="alert alert-danger white-text">';

                            foreach ($errors as $error)
                            {
                                echo $error . '<br></div>';
                            }

                        } else {
                            session_start();
                            header('Location: index.php?url=dashboard');
                        }
                    }
                    ?>
                    
        <!-- FORMULAIRE DE CONNEXION -->
        <form method="post" action="index.php?url=connecting">
                    <div class="input-field form-group">
                <input id="email" name="email" type="email" class="form-control" placeholder="Email" />
                <label for="email"></label>
            </div>
            <div class="form-group">
                <input id="password" name="password" type="password" class="form-control" placeholder="Mot de passe" />
                <label for="password"></label>
            </div>
            <div class="form-group">
                <input id='submit' type="submit" name="submit" class="w-100 btn btnSubmit btn-dark" />
            </div>
        </form>
    </div>
</div>
