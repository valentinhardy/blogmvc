<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include 'head.php'; ?>
</head>
<body>
<?php include('topbar.php');   ?>
<div class="top"></div>


<div class="<?php echo  $_GET['url']?>">
    <?php echo  $content; ?>
</div>

<script src="./assets/js/jquery.min.js"></script>
<script src="./assets/js/bootstrap.min.js"></script>
<script src="./assets/js/script.js"></script>
<script src="./assets/js/masonry.js"></script>
<script src="./assets/js/owl.carousel.min.js"></script>
<script src="https://stackpath.bootstrap/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
</body>
</html>  