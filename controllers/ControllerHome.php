<?php session_start();

/**
 *  home : get all articles where posted -> 1 (@ArticleManager <- Model)
 */


require_once('./views/view.php');

class ControllerHome {

    private $_articleManager;
    private $_view;

    public function home() {

        $this->_articleManager = new ArticleManager;
        $articles = $this->_articleManager->getArticles();

        $this->_view = new View('home');
        $this->_view->generate(array('articles' => $articles));
        
    }
}