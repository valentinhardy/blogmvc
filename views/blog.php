<?php //session_start(); ?>
<?php $this->_t = 'Jean Forteroche - Blog'; ?>

<!-- GRID -->
<div class="container-fluid">
    <div class="grid">
        <?php foreach($articles as $article):?>
        <div class="grid-item">
            <div class="blog-grid_content" style="background: url('./assets/img/<?php echo $article->image()?>');">
                <h4 class="text-center mb-2 mt-2"><?php echo  $article->title()?></h4>
                <a class="btn btn-dark blog-grid__btn mt-3 text-center" style="font-size:24px;" href="index.php?url=post&id=<?php echo  $article->id()?>">Voir l'article</a>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>