<?php session_start();

/**
 * Post-> Permet de récuperer un post avec son contenu selon son id, ainsi que ses commentaires
 * addComment-> Envoi un commentaire si pas d'erreurs
 */

 require_once('./views/view.php');

class ControllerPost {

    private $_postManager;
    private $_postCommentsManager;
    private $_view;
    
    
    
    // Recupère le post index.php?url=post&id=$id
    public function post($id) {

        $this->_postManager = new PostManager();
        $post = $this->_postManager->getPost($id);

            // If post exist & id = number
            if($post == true && is_numeric($_GET['id'])) {
                //if submit exist
                $errors = [];
                if(isset($_POST['submit'])){

                    //Send a comment
                    $name = htmlspecialchars(trim($_POST['name']));
                    $email   = htmlspecialchars(trim($_POST['email']));
                    $comment = htmlspecialchars(trim($_POST['comment']));
                    $submit =  $_POST['submit'];
                
                    $submit = 1;
                 
                    if(isset($name) || isset($email) || isset($comment)) {
        
                        if(empty($_POST['name'])) {
                            $errors['emptyTitle'] = 'Veuillez indiquer votre nom.'; 
                        }
        
                        if(empty($_POST['email'])) {
                            $errors['emptyEmail'] = 'Veuillez indiquer votre email.';
                        }
                        
                        if(empty($_POST['comment'])) {
                            $errors['emptyContent'] = 'Veuillez remplir le champ commentaire';
                        }
        
                        if(strlen($_POST['comment']) < 20) {
                            $errors['lowerContent'] = 'Commentaires : longueur minimum de 20 caractères.';
                        }
        
                        else if (empty($errors)){
                            // LETS GO SENDCOMMENT
                            $this->_postCommentsManager = new CommentsManager();
                            $this->_postCommentsManager->sendComment($id,$name,$email,$comment);
                            $submit = 0;                  
        
                        }
                            
                    } else {
        
                        die('Une erreur est survenue');
        
                    }
                    
                }

                //create Post View
                $this->_commentsManager = new CommentsManager();
                $comments = $this->_commentsManager->getComments($id);
        
                
                // Encode for TinyMCE // Todo : trouver le pb
                $post->content = html_entity_decode(ucfirst($post->content), ENT_HTML5 , 'UTF-8');

                $this->_view = new View('post');
                $this->_view->generate(array('post' => $post, 'comments' => $comments, 'errors' => $errors));


            // Else Error
            } else {

                $errorMsg = 'Cet article n\'existe pas.';
                $this->_view = new View('error');
                $this->_view->generate(array('errorMsg' => $errorMsg));
                
            }

    }
}
