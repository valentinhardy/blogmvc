<?php 
/**
    * Articles

    * getArticles : search table 'posts' for setting Article

 **/ 


 class ArticleManager extends Model {

    //recupere tous les articles
    public function getArticles(){
        $this->getBdd(); //function article manager 
        return $this->getAll('posts', 'Article');
    }

}